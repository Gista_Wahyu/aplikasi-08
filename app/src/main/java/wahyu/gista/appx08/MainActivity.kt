package wahyu.gista.appx08

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    lateinit var preferences : SharedPreferences
    val PREF_NAME = "setting"
    var BG_TITLE_COLOR = "bg_title_color"
    var FONT_TITLE_COLOR = "font_title_color"
    var FONT_TITLE_SIZE = "font_title__size"
    var TITLE_TEXT = "title_text"
    var DETAIL_FONT_SIZE = "detail_font_size"
    var DETAIL_TEXT = "detail_text"
    val DEF_TITLE_COLOR = "RED"
    val DEF_FONT_TITLE_COLOR  = "WHITE"
    val DEF_FONT_TITLE_SIZE = 24
    val DEF_TITLE_TEXT = "THE PLATFORM"
    val DEF_DETAIL_FONT_SIZE = 18
    val DEF_DETAIL_TEXT =
        "Ketika Agus hendak membantu Lilis mencari suaminya yang sudah tiga bulan tidak pulang dan tidak berkabar, Vespanya dicuri orang. Bersama teman-temannya sesama anak Vespa, Agus keliling Bandung mencari Vespanya dan juga suami Lilis. Tanpa sengaja, mereka terlibat dalam kasus yang melibatkan komplotan pencuri, geng motor, teroris, dan polisi."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        loadSetting()
    }

    fun loadSetting(){
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txTitle.setBackgroundColor(Color.parseColor(preferences.getString(BG_TITLE_COLOR,DEF_TITLE_COLOR)))
        txTitle.setTextColor(Color.parseColor(preferences.getString(FONT_TITLE_COLOR,DEF_FONT_TITLE_COLOR)))
        txTitle.textSize = preferences.getInt(FONT_TITLE_SIZE,DEF_FONT_TITLE_SIZE).toFloat()
        txDetail.textSize = preferences.getInt(DETAIL_FONT_SIZE,DEF_DETAIL_FONT_SIZE).toFloat()
        txTitle.setText(preferences.getString(TITLE_TEXT,DEF_TITLE_TEXT))
        txDetail.setText(preferences.getString(DETAIL_TEXT,DEF_DETAIL_TEXT))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.option_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemSetting ->{
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)
            }
        }
        return super.onOptionsItemSelected(item)
    }
}

